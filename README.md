This are Slic3r settings for *Anycubic Kossel Pulley* 3D printer on my desk.

Using the CaliCat as benchmark.

[Cali Cat - The Calibration Cat](https://www.thingiverse.com/thing:1545913) by [Dezign](https://www.thingiverse.com/Dezign) is licensed under the [Creative Commons - Attribution](http://creativecommons.org/licenses/by/3.0/) license