## Temperature
* PID-P: +022.20
* PID-I: +001.08
* PID-D: +114.80

## Velocity
* Vmax X: 200
* Vmax Y: 200
* Vmax Z: 200
* Vmax E: 25

## Acceleration
* Accel: 30
* A-retract: 3000
* A-travel: 30
* Amax X: 20
* Amax Y: 20
* Amax Z: 500
* Amax E: 10000

## Jerk
* Vx-jerk: 20
* Vy-jerk: 20
* Vz-jerk: 20
* Ve-jerk: 5

## Steppers
* Xsteps/mm:  80
* Ysteps/mm:  80
* Zsteps/mm:  80
* Esteps/mm: 100

## (Position) Delta Calibration
* Diag Rod: +217.00
* Height: +351.42
* Ex:  0
* Ey: -0.140
* Ez: -0.685
* Radius:  97.12
* Tx: -0.305
* Ty:  0.254
* Tz:  0
